package club.tazed.ChunkReLoader;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ChunkReLoaderPlugin extends JavaPlugin {
    private ChunkManager manager;

    public void onEnable() {
        manager = ChunkManager.getChunkManagerOf(Bukkit.getWorld("world"));

        Bukkit.getScheduler().runTaskTimer(this, () -> manager.updateChunks(), 0, 20);
    }
}
