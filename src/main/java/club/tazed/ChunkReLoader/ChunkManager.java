package club.tazed.ChunkReLoader;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class ChunkManager {
    private static HashMap<World, ChunkManager> instances = new HashMap<>();

    private HashMap<Chunk, Sign> chunks;
    private World world;

    private ChunkManager(@NotNull World world) {
        this.world = world;
        this.chunks = new HashMap<>();

        for (Chunk chunk : world.getLoadedChunks()) {
            for (BlockState blockState: chunk.getTileEntities()) {
                if (blockState instanceof Sign) {
                    Sign sign = (Sign) blockState;
                    registerSign(sign);
                }
            }
        }
    }

    public static ChunkManager getChunkManagerOf(@NotNull World world) {
        if (instances.containsKey(world)) {
            return instances.get(world);
        }
        ChunkManager manager = new ChunkManager(world);
        instances.put(world, manager);
        return manager;
    }

    public void updateChunks() {
        for (Sign sign : chunks.values()) {
            ChunkSign chunkSign = new ChunkSign(sign);
            chunkSign.update();
        }
    }

    public void registerSign(@NotNull Sign sign) {
        ChunkSign chunkSign = new ChunkSign(sign);
        if (chunkSign.isChunkSign()) {
            Chunk chunk = sign.getChunk();
            chunks.put(chunk, sign);
        }
    }
}
