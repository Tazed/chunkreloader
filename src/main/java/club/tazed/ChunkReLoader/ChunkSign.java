package club.tazed.ChunkReLoader;

import org.bukkit.block.Sign;
import org.jetbrains.annotations.NotNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChunkSign {
    private Sign sign;

    public ChunkSign(@NotNull Sign sign) {
        this.sign = sign;

        if (isChunkSign() && sign.getLine(1).isEmpty()) {
            setFuel(5000);
        }
    }

    public void update() {
        int fuel = getFuel();
        if (fuel > 0)
            setFuel(fuel - 1);
    }

    public boolean isChunkSign() {
        return sign.getLine(0).toLowerCase().equals("chunkloaded");
    }

    public int getFuel() {
        String line = sign.getLine(1);
        Matcher matcher = Pattern.compile("^Fuel: (\\d+)$")
                .matcher(line);
        matcher.find();
        if (matcher.groupCount() == 1) {
            return Integer.parseInt(matcher.group(1));
        }
        return setFuel(0);
    }

    public int setFuel(int fuel) {
        sign.setLine(1, String.format("Fuel: %d", fuel));
        sign.update(true);
        return fuel;
    }
}
